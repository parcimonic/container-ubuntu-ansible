# Ubuntu container

This is a [Ubuntu](https://ubuntu.com/) 22.04 based image that I'm using for my
[Molecule](https://molecule.readthedocs.io/en/latest/index.html) tests.

URL: `registry.gitlab.com/parcimonic/container-ubuntu-ansible:latest`
