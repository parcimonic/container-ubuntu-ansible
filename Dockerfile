FROM ubuntu:22.04

# Add sudo so we can `become_user` in Ansible.
# hadolint ignore=DL3008
RUN set -euf && \
    apt-get update && \
    apt-get install --yes --no-install-recommends \
    python3 \
    sudo \
    systemd && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Add a non-root user for some Ansible tasks that
# target unprivileged users.
RUN useradd -m milkyway
